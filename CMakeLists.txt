project(TED)

cmake_minimum_required(VERSION 2.6)
if(COMMAND cmake_policy)
	cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)
SET(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules;${CMAKE_MODULE_PATH}")

set(EXECUTABLE main)
set(TED_SRC src)
file(GLOB_RECURSE TED_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "${TED_SRC}/*.c*")

find_package(SDL REQUIRED)
find_package(SDL_image REQUIRED)
find_package(OpenGL REQUIRED)
#find_package(FTGL REQUIRED)

if(CMAKE_SYSTEM_NAME MATCHES Windows)
	#this is to fix some cross compiling issues I'm having
	#set(SDL_INCLUDE_DIR FTGL_INCLUDE_DIR)
	#set(SDLIMAGE_INCLUDE_DIR FTGL_INCLUDE_DIR)
endif(CMAKE_SYSTEM_NAME MATCHES Windows)

include_directories(
    ${CMAKE_SOURCE_DIR}/${TED_SRC}
    ${SDL_INCLUDE_DIR}
    ${SDLIMAGE_INCLUDE_DIR}  
    ${OPENGL_INCLUDE_DIR}
    #${FREETYPE_INCLUDE_DIRS}
    #${FTGL_INCLUDE_DIR}
)

link_libraries(
    #${FTGL_LIBRARY}
    ${FREETYPE_LIBRARIES}
    ${OPENGL_LIBRARIES}
    ${SDL_LIBRARY}
    ${SDLIMAGE_LIBRARY}
)

add_definitions(
    -pipe -Wall -D_REENTRANT -D_GNU_SOURCE=1
)

if(BUILD MATCHES "Debug")
	add_definitions(-DDEBUG -g)
	message(STATUS "DEBUGGING ON!")
	if(PROFILE)
		add_definitions(-pg)
		link_libraries(-pg)
		message(STATUS "PROFILING ON!")
	endif(PROFILE)
endif(BUILD MATCHES "Debug")

if(BUILD MATCHES "Debug")
	add_definitions(-03)
endif(BUILD MATCHES "Debug")

add_executable(${EXECUTABLE} ${TED_FILES})
