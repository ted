#ifndef TERRAINDISPLAY_H
#define TERRAINDISPLAY_H

#include "../src/terrain.h"

#ifdef __cplusplus
extern "C++" {
#endif 

#include <QtOpenGL/QGLWidget>

class TerrainDisplay : public QGLWidget
{
    Q_OBJECT

    public:
        TerrainDisplay(QWidget *parent = 0);
        ~TerrainDisplay();

        QSize minimumSizeHint() const;
        QSize sizeHint() const;

        float getZoom() { return m_zoom; }

        float getXRotation() { return m_rot[0]; }
        float getYRotation() { return m_rot[1]; }
        float getZRotation() { return m_rot[2]; }

        float getRoughness() { return m_roughness; }
        float getMagnitude() { return m_magnitude; }

        int getSize() { return m_mesh->size; }
        int getSeed() { return m_mesh->seed; }

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();
        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);

    public slots:
        void setZoom(double amount);

        void setXRotation(double angle);
        void setYRotation(double angle);
        void setZRotation(double angle);

        void setRoughness(double rough);
        void setMagnitude(double mag);

        void setSize(int size);
        void setSeed(int seed);
 
        void setDrawMode(int mode);

        void zoomFit();
        void makeFractal();

    signals:
        void zoomChanged(double amount);

        void xRotationChanged(double angle);
        void yRotationChanged(double angle);
        void zRotationChanged(double angle);

        void roughnessChanged(double rough);
        void magnitudeChanged(double mag);

        void sizeChanged(int size);
        void seedChanged(int seed);

    private:
        bool m_controllable;

        double m_zoom, m_maxZoom;
        double m_rot[3];
        double m_mouseSensitivity;

        QPoint m_lastPos;

        double m_roughness;
        double m_magnitude;
        struct terrain *m_mesh;
};

#ifdef __cplusplus
}
#endif 

#endif // TERRAINDISPLAY_H
