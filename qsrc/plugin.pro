TEMPLATE = lib
CONFIG += designer plugin debug_and_release opengl
QT += opengl

CONFIG(debug, debug|release) {
	mac: TARGET = $$join(TARGET,,,_debug);
	win32: TARGET = $$join(TARGET,,d);
}

CONFIG(release, debug|release) {
        QMAKE_CXXFLAGS += -Wall -O2
        QMAKE_LFLAGS += -s
}

HEADERS = ../src/terrain.h \
          terraindisplay.h \
          widgetplugin.h
SOURCES = ../src/terrain.c \
          ../src/terrain_frac.c \
          terraindisplay.cpp \
          widgetplugin.cpp

UI_DIR = build/uic
OBJECTS_DIR = build/obj
MOC_DIR = build/moc
DESTDIR = build/bin

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS += target
