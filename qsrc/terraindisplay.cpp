#include "terraindisplay.h"

#include <QtGui/QMessageBox>
#include <QtGui/QMouseEvent>
//#include <QtScript/QScriptEngine>
//#include <QtCore/QDebug>

#include <iostream>

#include <GL/gl.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#define DEFAULT_SIZE 64

TerrainDisplay::TerrainDisplay(QWidget *parent) : QGLWidget(parent)
{
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

    m_mesh = terrain_create(DEFAULT_SIZE);

    setZoom(10.0);

    m_mouseSensitivity = 0.5;

    setXRotation(45.0);
    setYRotation(45.0);
    emit zRotationChanged(0.0); // to fill in the UI

    makeCurrent();

    setSize(DEFAULT_SIZE);
    setSeed(time(0));
    setMagnitude(DEFAULT_SIZE);
    setRoughness(0.65);
    zoomFit();
}

TerrainDisplay::~TerrainDisplay()
{
    makeCurrent();
    terrain_destroy(m_mesh);
}

QSize TerrainDisplay::minimumSizeHint() const
{
    return QSize(64, 64);
}

QSize TerrainDisplay::sizeHint() const
{
    return QSize(320, 320);
}

void TerrainDisplay::initializeGL()
{
    glClearColor(0.0, 0.0, 0.0, 0.0);    

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CW);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CW);

    glShadeModel(GL_SMOOTH);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

/*
    float position[] = { 10.0f, 10.0f, 10.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_POSITION, position);

    float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

    float ambient[] =  { 0.0f, 0.0f, 0.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);

    float diffuse[] =  { 1.0f, 1.0f, 1.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;

    glMaterialf(GL_FRONT, GL_SHININESS, 128);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
*/
}

void TerrainDisplay::resizeGL(int w, int h)
{
    float ratio = (float)w/h;
    float scale = 10.0;

    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-scale*ratio, scale*ratio, -scale, scale, -1000.0, 1000.0);
    glMatrixMode(GL_MODELVIEW);
}

void TerrainDisplay::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, -10.0);
    glRotated(m_rot[0], 1.0, 0.0, 0.0);
    glRotated(m_rot[1], 0.0, 1.0, 0.0);
    glRotated(m_rot[2], 0.0, 0.0, 1.0);

    glScalef(m_zoom, m_zoom, m_zoom);
    glColor3f(1.0, 1.0, 1.0);
    terrain_draw(m_mesh);

    glPushMatrix();

    glScalef(1.0/m_zoom, 1.0/m_zoom, 1.0/m_zoom);

    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);

    glBegin(GL_LINES);

    glColor3f(1.0, 0.0, 0.0); // x
    glVertex3f(0.0, 0.0, 0.0); 
    glVertex3f(1.0, 0.0, 0.0);

    glColor3f(0.0, 1.0, 0.0); // y
    glVertex3f(0.0, 0.0, 0.0); 
    glVertex3f(0.0, 1.0, 0.0);

    glColor3f(0.0, 0.0, 1.0); // z
    glVertex3f(0.0, 0.0, 0.0); 
    glVertex3f(0.0, 0.0, 1.0);

    glEnd();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);

    glPopMatrix();
}

void TerrainDisplay::mousePressEvent(QMouseEvent *event)
{
    m_lastPos = event->pos();
}

void TerrainDisplay::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - m_lastPos.x();
    int dy = event->y() - m_lastPos.y();

    if (event->buttons() & Qt::LeftButton)
    {
        setXRotation(getXRotation() + m_mouseSensitivity * dy);
        setYRotation(getYRotation() + m_mouseSensitivity * dx);
    }
    else if (event->buttons() & Qt::RightButton)
    {
        setZoom(getZoom() + m_mouseSensitivity/100.0 * dy * getZoom());
    }
    else if (event->buttons() & Qt::MidButton)
    {
        makeCurrent();
        makeFractal();
        updateGL();
    }

    m_lastPos = event->pos();
}

void TerrainDisplay::zoomFit()
{
    setZoom(10.0/getSize());
}


void TerrainDisplay::setZoom(double amount)
{
    if (amount != m_zoom && amount > 0.0)
    {
        m_zoom = amount;
        emit zoomChanged(amount);
        updateGL();
    }
}

void normalizeAngle(double *angle)
{
    //int adj = *angle/360;
    //*angle -= adj*360;
}

void TerrainDisplay::setXRotation(double angle)
{
    normalizeAngle(&angle);
    if (angle != m_rot[0])
    {
        m_rot[0] = angle;
        emit xRotationChanged(angle);
        updateGL();
    }
}

void TerrainDisplay::setYRotation(double angle)
{
    normalizeAngle(&angle);
    if (angle != m_rot[1])
    {
        m_rot[1] = angle;
        emit yRotationChanged(angle);
        updateGL();
    }
}

void TerrainDisplay::setZRotation(double angle)
{
    normalizeAngle(&angle);
    if (angle != m_rot[2])
    {
        m_rot[2] = angle;
        emit zRotationChanged(angle);
        updateGL();
    }
}

void TerrainDisplay::setRoughness(double rough)
{
    if (rough != m_roughness)
    {
        m_roughness = rough;
        emit roughnessChanged(rough);
        makeFractal();
        updateGL();
    }
}

void TerrainDisplay::setMagnitude(double mag)
{
    if (mag != m_magnitude)
    {
        m_magnitude = mag;
        emit magnitudeChanged(mag);
        makeFractal();
        updateGL();
    }
}

void TerrainDisplay::setSize(int size)
{
    if (size != m_mesh->size)
    {
        TERRAIN_MODE m = m_mesh->drawmode;
        terrain_destroy(m_mesh);
        m_mesh = terrain_create(size); 
        m_mesh->drawmode = m;
        makeFractal();
        emit sizeChanged(size);
        zoomFit();
        updateGL();
    }
}

void TerrainDisplay::setDrawMode(int mode)
{
    switch(mode)
    {
        case 0:
            m_mesh->drawmode = FLAT;
            m_mesh->dirty = 1;
            break;
        case 1:
            m_mesh->drawmode = NORMAL;
            m_mesh->dirty = 1;            
            break;
        case 2:
            m_mesh->drawmode = NORMAL_DEBUG;
            m_mesh->dirty = 1;            
            break;
        default:
            break;
    }
    updateGL();
}

void TerrainDisplay::setSeed(int seed)
{
    if ((unsigned int)seed != m_mesh->seed)
    {
        m_mesh->seed = seed;
        makeFractal();
        emit seedChanged(seed);
        updateGL();
    }
}

void TerrainDisplay::makeFractal()
{
    terrain_clear(m_mesh);
    terrain_fractal(m_mesh, m_magnitude);
    terrain_smooth(m_mesh, m_roughness);
    terrain_bake(m_mesh);
}

