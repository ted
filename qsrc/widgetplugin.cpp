#include "widgetplugin.h"

#include "terraindisplay.h"
#include <QtCore/QtPlugin>

TerrainDisplayPlugin::TerrainDisplayPlugin(QObject *parent) : QObject(parent)
{
    initialized = false;
}

void TerrainDisplayPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (initialized) return;

    initialized = true;
}

bool TerrainDisplayPlugin::isInitialized() const
{
    return initialized;
}

QWidget *TerrainDisplayPlugin::createWidget(QWidget *parent)
{
    return new TerrainDisplay(parent);
}

QString TerrainDisplayPlugin::name() const
{
    return "TerrainDisplay";
}

QString TerrainDisplayPlugin::group() const
{
    return "Display Widgets";
}

QIcon TerrainDisplayPlugin::icon() const
{
    return QIcon();
}

QString TerrainDisplayPlugin::toolTip() const
{
    return "Display widget for TED";
}

QString TerrainDisplayPlugin::whatsThis() const
{
    return "";
}

bool TerrainDisplayPlugin::isContainer() const
{
    return false;
}

QString TerrainDisplayPlugin::domXml() const
{
	return "<widget class=\"TerrainDisplay\" name=\"terrainDisplay\">\n"
			" <property name=\"geometry\">\n"
			"  <rect>\n"
			"   <x>0</x>\n"
			"   <y>0</y>\n"
			"   <width>320</width>\n"
			"   <height>320</height>\n"
			"  </rect>\n"
			" </property>\n"
			" <property name=\"toolTip\" >\n"
			"  <string>Terrain Display</string>\n"
			" </property>\n"
			" <property name=\"whatsThis\" >\n"
			"  <string>The Terrain Display shows a preview "
			"of the current terrain being edited.</string>\n"
			" </property>\n"
			"</widget>\n";
}

QString TerrainDisplayPlugin::includeFile() const
{
    return "terraindisplay.h";
}

Q_EXPORT_PLUGIN2(customwidgetplugin, TerrainDisplayPlugin)

