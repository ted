#include "terrain.h"

#include <stdlib.h>

float noise_white()
{
    return (float)rand()/RAND_MAX - 0.5;
}

static void pass(struct index *ul, struct index *ur,
                 struct index *ll, struct index *lr,
                 struct terrain *t, float mag, float adj)
{
    int size = t->size;
    float *height = t->height;
    struct index ml, mu, mr, md, m;

    m.i = (ul->i + lr->i) * 0.5;
    m.j = (ul->j + lr->j) * 0.5;

    ml.i = ul->i;
    ml.j = (ul->j + ll->j) * 0.5;

    mu.i = (ul->i + ur->i) * 0.5;
    mu.j = ul->j;

    mr.i = ur->i;
    mr.j = (ur->j + lr->j) * 0.5;

    md.i = (ll->i + lr->i) * 0.5;
    md.j = ll->j;

    // middle
    int idx = m.i*size + m.j;
    float av = height[idx];

    av += height[ul->i*size + ul->j];
    av += height[ur->i*size + ur->j];
    av += height[ll->i*size + ll->j];
    av += height[lr->i*size + lr->j];
    av *= 0.25;

    height[idx] = av + noise_white() * mag;
    if (height[idx] > t->max_height) t->max_height = height[idx];
    if (height[idx] < t->min_height) t->min_height = height[idx];

    mag *= adj;

    // left
    idx = ml.i*size + ml.j;
    av = 0;

    av += height[ul->i*size + ul->j];
    av += height[ml.i*size + ml.j];
    av += height[m.i*size + m.j];
    av += height[ll->i*size + ll->j];
    av *= 0.25;

    height[idx] = av + noise_white() * mag;
    if (height[idx] > t->max_height) t->max_height = height[idx];
    if (height[idx] < t->min_height) t->min_height = height[idx];

    // right
    idx = mr.i*size + mr.j;
    av = 0;

    av += height[ur->i*size + ur->j];
    av += height[mr.i*size + mr.j];
    av += height[m.i*size + m.j];
    av += height[lr->i*size + lr->j];
    av *= 0.25;

    height[idx] = av + noise_white() * mag;
    if (height[idx] > t->max_height) t->max_height = height[idx];
    if (height[idx] < t->min_height) t->min_height = height[idx];

    // up
    idx = mu.i*size + mu.j;
    av = 0;

    av += height[ul->i*size + ul->j];
    av += height[mu.i*size + mu.j];
    av += height[ur->i*size + ur->j];
    av += height[m.i*size + m.j];
    av *= 0.25;

    height[idx] = av + noise_white() * mag;
    if (height[idx] > t->max_height) t->max_height = height[idx];
    if (height[idx] < t->min_height) t->min_height = height[idx];

    // down
    idx = md.i*size + md.j;
    av = 0;

    av += height[ll->i*size + ll->j];
    av += height[md.i*size + md.j];
    av += height[m.i*size + m.j];
    av += height[lr->i*size + lr->j];
    av *= 0.25;

    height[idx] = av + noise_white() * mag;
    if (height[idx] > t->max_height) t->max_height = height[idx];
    if (height[idx] < t->min_height) t->min_height = height[idx];

    if (m.i != ul->i && m.j != ul->j)
    {
        pass(ul, &mu, &ml, &m, t, mag, adj); // left up
        pass(&mu, ur, &m, &mr, t, mag, adj); // right up
        pass(&ml, &m, ll, &md, t, mag, adj); // left down
        pass(&m, &mr, &md, lr, t, mag, adj); // right down
    }
}


void pass_plasma(int x, int y, int width, int height, 
                 float a, float b, float c, float d,
                 struct terrain *t)
{
    int nwidth = width * 0.5;
    int nheight = height * 0.5;

    if (width > 1 || height > 1)
    {
        //float disp = (float)(nwidth+nheight) / (width+height) * 3.0;

        int m = (a+b+c+d) * 0.25 + noise_white() * 10;

        int a1 = (a+b) * 0.5;
        int b1 = (b+c) * 0.5;
        int c1 = (c+d) * 0.5;
        int d1 = (d+a) * 0.5;
        
        pass_plasma(x, y, nwidth, nheight, a, a1, m, d1, t);
        pass_plasma(x + nwidth, y, nwidth, nheight, a1, b, b1, m, t);
        pass_plasma(x + nwidth, y + nheight, nwidth, nheight, m, b1, c, c1, t);
        pass_plasma(x, y + nheight, nwidth, nheight, d1, m, c1, d, t);
    }
    else
    {
        float av = (a+b+c+d) * 0.25;
        t->height[x*t->size + y] = av;
        if (av < t->min_height) t->min_height = av;
        else if (av > t->max_height) t->max_height = av;
    }
}


void terrain_fractal(struct terrain *t, float mag)
{
    int size = t->size;

    float adj = 0.5;

    struct index ul = {0, 0};
    struct index ur = {size-1, 0};
    struct index ll = {0, size-1};
    struct index lr = {size-1, size-1};
    
    /*
    t->height[ul.i*size + ul.j] = noise_white() * mag;
    t->height[ur.i*size + ur.j] = noise_white() * mag;
    t->height[ll.i*size + ll.j] = noise_white() * mag;
    t->height[lr.i*size + lr.j] = noise_white() * mag;
    */
    
    t->max_height = 1.0;
    t->min_height = 0.0;
    t->dirty = 1;

    srand(t->seed);

    pass(&ul, &ur, &ll, &lr, t, mag, adj);
}

void terrain_plasma(struct terrain *t)
{
    int size = t->size;
 
    srand(t->seed);

    t->max_height = 1.0;
    t->min_height = 0.0;
    t->dirty = 1;

    pass_plasma(0, 0, size, size, noise_white(), noise_white(), noise_white(), noise_white(), t);
}

