#include "terrain.h"
#include <stdlib.h>
#include <math.h>
#include <string.h> // for memset

struct terrain* terrain_create(int size)
{
    struct terrain *t = malloc(sizeof(struct terrain));
    t->size = size;
    t->height = malloc(sizeof(float)*size*size);
    t->normal = malloc(sizeof(struct point)*size*size);
    t->drawmode = FLAT;
    t->drawlist = glGenLists(1);
    t->dirty = 1;
    t->max_height = 1.0;
    return t;
}

void terrain_destroy(struct terrain* t)
{
    free(t->height);
    free(t->normal);
    glDeleteLists(t->drawlist, 1);
    free(t);
}

inline static void calculate_normal(int ai, int aj, int bi, int bj, int ci, int cj,
                                    float *height, struct point *normal, int size)
{
    struct point a = {ai, height[ai*size+aj], aj, 0.0};
    struct point b = {bi, height[bi*size+bj], bj, 0.0};
    struct point c = {ci, height[ci*size+cj], cj, 0.0};

    b.x -= a.x;
    b.y -= a.y;
    b.z -= a.z;

    c.x -= a.x;
    c.y -= a.y;
    c.z -= a.z;

    struct point *n = &normal[ai*size+aj];

    point_cross(n, &b, &c);
    point_norm(n);
}

void terrain_bake(struct terrain *t)
{
    int size = t->size;
    float *height = t->height;
    struct point *normal = t->normal;
    struct point tmp, *ptr;

    t->dirty = 1;

    // calculate the inside
    int i, j;
    for (i=1; i<size-1; i++)
    {
        for (j=1; j<size-1; j++)
        {
            ptr = &normal[i*size+j];
            calculate_normal(i,j, i,j+1, i+1,j, height, normal, size);
            tmp.x = ptr->x;
            tmp.y = ptr->y;
            tmp.z = ptr->z;
            calculate_normal(i,j, i+1,j, i,j-1, height, normal, size);
            tmp.x += ptr->x;
            tmp.y += ptr->y;
            tmp.z += ptr->z;
            calculate_normal(i,j, i,j-1, i-1,j, height, normal, size);
            tmp.x += ptr->x;
            tmp.y += ptr->y;
            tmp.z += ptr->z;
            calculate_normal(i,j, i-1,j, i,j+1, height, normal, size);
            tmp.x += ptr->x;
            tmp.y += ptr->y;
            tmp.z += ptr->z;

            tmp.x *= 0.25;
            tmp.y *= 0.25;
            tmp.z *= 0.25;
            ptr->x = tmp.x;
            ptr->y = tmp.y;
            ptr->z = tmp.z;
        }
    }

    // calculate the outer ridge
    i=size-1;
    for (j=0; j<size-1; j++)
    {
        calculate_normal(0,j, 0,j+1, 1,j, height, normal, size);
        calculate_normal(i,j, i-1,j, i,j+1, height, normal, size);
        calculate_normal(j,0, j,1, j+1,0, height, normal, size);
        calculate_normal(j,i, j+1,i, j,i-1, height, normal, size);
    }
}

void build_execute(struct terrain *t)
{
    glNewList(t->drawlist, GL_COMPILE_AND_EXECUTE);

    int size = t->size;
    float *height = t->height;
    struct point *normal = t->normal;

    struct point *n;
    float z, zcol;

    float div = t->max_height - t->min_height;
    if (div == 0) div = 0.000001;
    float scale = 1.0/div;

    int i, j;
    for (i=0; i<size-1; i++)
    {
        glBegin(GL_TRIANGLE_STRIP);
        for (j=0; j<size; j++)
        {
            z = height[i*size+j];
            zcol = (z - t->min_height)*scale;

            if (t->drawmode == FLAT)
                glColor3f(0.0, zcol, 1.0 - zcol);

            if (t->drawmode == NORMAL || t->drawmode == NORMAL_DEBUG)
            {
                n = &normal[i*size+j];
                glNormal3f(n->x, n->y, n->z);
            }

            glVertex3f(i, z, j);

            z = height[(i+1)*size+j];
            zcol = (z - t->min_height)*scale;

            if (t->drawmode == FLAT)
                glColor3f(0.0, zcol, 1.0 - zcol);

            if (t->drawmode == NORMAL || t->drawmode == NORMAL_DEBUG)
            {
                n = &normal[(i+1)*size+j];
                glNormal3f(n->x, n->y, n->z);
            }

            glVertex3f(i+1, z, j);
        }
        glEnd();
    }

    if (t->drawmode == NORMAL_DEBUG)
    {
        glDisable(GL_LIGHTING);
        glColor3f(1.0, 0.0, 0.0);
        glBegin(GL_LINES);
        for (i=0; i<size; i++)
        {
            for (j=0; j<size; j++)
            {
                struct point p = {i, height[i*size+j], j, 0.0};
                glVertex3f(p.x, p.y, p.z);
                point_add(&p, &normal[i*size+j]);
                glVertex3f(p.x, p.y, p.z);
            }
        }
        glEnd();
        glEnable(GL_LIGHTING);
    }

    glEndList();
}

void terrain_draw(struct terrain *t)
{
    int size = t->size;

    glPushMatrix();
    glTranslatef(-0.5*size, 0.0, -0.5*size);

    if (t->drawmode == FLAT)
        glDisable(GL_LIGHTING);

    if (t->dirty)
    {
       build_execute(t);
       t->dirty = 0;
    }
    else
    {
       glCallList(t->drawlist);
    }

    glEnable(GL_LIGHTING);

    glPopMatrix();
}

void terrain_smooth(struct terrain *t, float k)
{
    int size = t->size;
    float *height = t->height;

    t->dirty = 1;

    int i, j, idx;
    // left to right
    for (i=1; i<size; i++)
    {
        for (j=0; j<size; j++)
        {
            idx = i*size+j;
            height[idx] = height[(i-1)*size+j] * (1.0-k) + height[idx] * k;
        }
    }
    // right to left
    for (i=size-2; i>-1; i--)
    {
        for (j=0; j<size; j++)
        {
            idx = i*size+j;
            height[idx] = height[(i+1)*size+j] * (1.0-k) + height[idx] * k;
        }
    }
    // bottom to top
    for (i=0; i<size; i++)
    {
        for (j=1; j<size; j++)
        {
            idx = i*size+j;
            height[idx] = height[i*size+j-1] * (1.0-k) + height[idx] * k;
        }
    }
    // top to bottom
    for (i=0; i<size; i++)
    {
        for (j=size-2; j>-1; j--)
        {
            idx = i*size+j;
            height[idx] = height[i*size+j+1] * (1.0-k) + height[idx] * k;
        }
    }
}

void terrain_clear(struct terrain *t)
{
    t->max_height = 1.0;
    t->min_height = 0.0;
    memset(t->height, 0, sizeof(float)*t->size*t->size);
}

