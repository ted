#include "backend/video.h"
#include "backend/input.h"
#include "backend/shapes.h"
#include "terrain.h"
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define MESH_SIZE 128

void terrain_demo(struct terrain *t)
{
    int size = t->size;
    float *height = t->height;

    int i, j;
    for (i=0; i<size; i++)
    {
        for (j=0; j<size; j++)
        {
            float z = (cos(i*0.5) + sin(j*0.5)) * 2.0;
            height[i*size+j] = z;
            if (z > t->max_height) 
                t->max_height = z;
            if (z < t->min_height)
                t->min_height = z;
        }
    }
}

void setup_gl();

int main(int argc, char **argv)
{
    v_init();
    v_closable(1); // can close the window when it's running

    v_setup(800, 600, 0);

    i_init();
    keystate_t *key;
    mousestate_t *mouse;

    shapes_init();

    struct terrain *mesh = terrain_create(MESH_SIZE);

    terrain_demo(mesh);
    terrain_bake(mesh);

    setup_gl();

    float zoom = 1.0;
    float zoom_min = 1.0;
    float zoom_max = 10.0;

    float rot[] = {45.0, -45.0, 0.0};

    float mouse_sensitivity = 0.2;

    int done = 0;
    while (!done)
    {
        i_pump();
        key = i_keystate();
        mouse = i_mousestate();

        if (key[K_ESCAPE]) done = 1;
        if (key[K_SPACE])
        {
            mesh->seed = time(0);
            terrain_clear(mesh);
            terrain_fractal(mesh, MESH_SIZE);
            terrain_bake(mesh);
        }
        if (key[K_p])
        {
            mesh->seed = time(0);
            terrain_plasma(mesh);
            terrain_bake(mesh);
        }
        if (key[K_s])
        {
            terrain_smooth(mesh, 0.90);
            terrain_bake(mesh);
        }
        if (key[K_f])
        {
            mesh->drawmode = FLAT;
            mesh->dirty = 1;
        }
        if (key[K_n])
        {
            terrain_bake(mesh);
            mesh->drawmode = NORMAL;
            mesh->dirty = 1;
        }
        if (key[K_d])
        {
            mesh->drawmode = NORMAL_DEBUG;
            mesh->dirty = 1;
        }

        if (mouse->button & M_BUTTON(M_MIDDLE))
        {
            zoom -= mouse->rel_y  * mouse_sensitivity * 0.1 * zoom;
            if (zoom < zoom_min) zoom = zoom_min;
            if (zoom > zoom_max) zoom = zoom_max;
        }
        if (mouse->button & M_BUTTON(M_RIGHT))
        {
            rot[1] += mouse->rel_x * mouse_sensitivity;
            rot[0] += mouse->rel_y * mouse_sensitivity;
        }

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glTranslatef(0.0, 0.0, -1000.0); // remember: this is an ortho projection
        glScalef(1.0/zoom, 1.0/zoom, 1.0/zoom);
        glRotatef(rot[0], 1,0,0);
        glRotatef(rot[1], 0,1,0);
        glRotatef(rot[2], 0,0,1);

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

        terrain_draw(mesh);

        glPushMatrix();
        {
            glScalef(zoom, zoom, zoom);

            glDisable(GL_LIGHTING);
            glDisable(GL_DEPTH_TEST);

            glBegin(GL_LINES);

            glColor3f(1.0, 0.0, 0.0); // x
            glVertex3f(0.0, 0.0, 0.0); 
            glVertex3f(1.0, 0.0, 0.0);

            glColor3f(0.0, 1.0, 0.0); // y
            glVertex3f(0.0, 0.0, 0.0); 
            glVertex3f(0.0, 1.0, 0.0);

            glColor3f(0.0, 0.0, 1.0); // z
            glVertex3f(0.0, 0.0, 0.0); 
            glVertex3f(0.0, 0.0, 1.0);

            glEnd();

            glEnable(GL_DEPTH_TEST);
            glEnable(GL_LIGHTING);
        }
        glPopMatrix();

        v_flip();
    }

    terrain_destroy(mesh);

    return 0;
}

void setup_gl()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    int width = v_info()->width;
    int height = v_info()->height;

    float ratio = (float)width/height;
    float scale = 10.0;

    glOrtho(-scale*ratio, scale*ratio, -scale, scale, -10000.0, 10000.0);
    
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}

