/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "texture.h"
#include "error.h"

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL_image.h>

void t_init()
{
	static int init = 0;
	if (init) return;
	init = 1;
	
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	//glScalef(-1,1,1);
}

struct texture *t_create()
{
	struct texture *t = (struct texture*)malloc(sizeof(struct texture));
	
	t->ready = 0;
	t->gl_handle = 0;
	
	return t;
}

void t_destroy(struct texture *t)
{
	t_unload(t);
	free(t);
}

int t_ready(struct texture *t)
{
	return t->ready;
}

void t_load(struct texture *t, const char *filename)
{
	t_unload(t);
	t->ready = 0;
	
	SDL_Surface *img = IMG_Load(filename);
	
	if (!img)
	{
		fprintf(stderr, "Texture load error: %s\n", IMG_GetError());
		return;
	}
	
	if (img->format->BytesPerPixel < 2)
	{
		fprintf(stderr, "Texture load error: %s is not true color\n", filename);
		return;
	}
	
	glGenTextures(1, &t->gl_handle);
	glBindTexture(GL_TEXTURE_2D, t->gl_handle);
	
	t->width = img->w;
	t->height = img->h;
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	GLuint mode = GL_RGB;
	if (img->format->BytesPerPixel == 4) mode = GL_RGBA;
	
	int mipmap = 0;
	
	if (!mipmap)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, img->format->BytesPerPixel, 
					 img->w, img->h, 0, mode, GL_UNSIGNED_BYTE, img->pixels);
	}
	else
	{
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );		
		gluBuild2DMipmaps(GL_TEXTURE_2D, img->format->BytesPerPixel, img->w, img->h,
						  mode, GL_UNSIGNED_BYTE, img->pixels);
	}
    
    SDL_FreeSurface(img);
	
	t->ready = 1;
}

void t_unload(struct texture *t)
{
	if (t_ready(t))
	{
		glDeleteTextures(1, &t->gl_handle);
	}
}

void t_apply(struct texture *t)
{
	glBindTexture(GL_TEXTURE_2D, t->gl_handle);
}

void t_set(struct texture *t, GLuint name, GLuint value)
{
	t_apply(t);
	glTexParameteri(GL_TEXTURE_2D,name,value);
}
