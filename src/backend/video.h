/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef VIDEO_H_
#define VIDEO_H_

#ifdef __cplusplus
extern "C" {
#endif 

struct video_info
{
    int width, height, bpp, flags, fullscreen;
};

const struct video_info* v_info();

void v_init();

void v_closable(int isclosable);

void v_setup(int width, int height, int fullscreen);

void v_flip();

double *v_unproject(float x, float y, float z);

#ifdef __cplusplus
}
#endif 

#endif
