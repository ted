/************************************************************************
    This file is part of NE.

    NE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NE.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#ifndef VEC_H_
#define VEC_H_

template<class T>
class Vec
{
public:
	Vec(T v=0) : m_x(v), m_y(v), m_z(v) {}
	Vec(T x, T y, T z) : m_x(x), m_y(y), m_z(z) {}
	
	///////////////////////
	// Set/Get functions //
	///////////////////////
	inline const Vec<T> set(T v)
	{
		m_x = m_y = m_z = v;
		return *this;
	}
	inline const Vec<T> &set(T x, T y, T z)
	{
		m_x = x; m_y = y; m_z = z;
		return *this;
	}
	inline const T* get()
	{
		static T v[3];
		v[0] = m_x;
		v[1] = m_y;
		v[2] = m_z;
		return &v;
	}
	inline T x() { return m_x; }
	inline T y() { return m_y; }
	inline T z() { return m_z; }
	
	///////////////////////
	// Equality overload //
	///////////////////////
	inline const Vec<T> &operator = (const Vec<T> &q)
	{
		return set(q.m_x,q.m_y,q.m_z);
	}
	inline bool operator == (const Vec<T> &q)
	{
		return m_x==q.m_x && m_y==q.m_y && m_z==q.m_z;
	}

	/////////////////////
	// Vector overload //
	/////////////////////
	inline const Vec<T> &operator + (const Vec<T> &v) const
	{
		return Vec<T>(m_x+v.m_x, m_y+v.m_y, m_z+v.m_z);
	}
	inline const Vec<T> &operator += (const Vec<T> &v)
	{
		return set(m_x+v, m_y+v, m_z+v);
	}
	
	inline const Vec<T> &operator - (const Vec<T> &v) const
	{
		return Vec<T>(m_x-v.m_x, m_y-v.m_y, m_z-v.m_z);
	}
	inline const Vec<T> &operator -= (const Vec<T> &v)
	{
		return set(m_x-v, m_y-v, m_z-v);
	}
		
	inline const Vec<T> &operator * (const Vec<T> &v) const
	{
		return Vec<T>(m_x*v.m_x, m_y*v.m_y, m_z*v.m_z);
	}
	inline const Vec<T> &operator *= (const Vec<T> &v)
	{
		return set(m_x*v, m_y*v, m_z*v);
	}
	
	inline const Vec<T> &operator / (const Vec<T> &v) const
	{
		return Vec<T>(m_x/v.m_x, m_y/v.m_y, m_z/v.m_z);
	}
	inline const Vec<T> &operator /= (const Vec<T> &v)
	{
		return set(m_x/v, m_y/v, m_z/v);
	}

	/////////////////////
	// Scalar overload //
	/////////////////////
	inline const Vec<T> &operator + (T v) const
	{
		return *this + Vec<T>(v);
	}
	inline const Vec<T> &operator += (T v)
	{
		return *this += Vec<T>(v);
	}
	
	inline const Vec<T> &operator - (T v) const
	{
		return *this - Vec<T>(v);
	}
	inline const Vec<T> &operator -= (T v)
	{
		return *this -= Vec<T>(v);
	}
		
	inline const Vec<T> &operator * (T v) const
	{
		return *this * Vec<T>(v);
	}
	inline const Vec<T> &operator *= (T v)
	{
		return *this *= Vec<T>(v);
	}
	
	inline const Vec<T> &operator / (T v) const
	{
		return *this / Vec<T>(v);
	}
	inline const Vec<T> &operator /= (T v)
	{
		return *this /= Vec<T>(v);
	}
		
protected:
	T m_x, m_y, m_z;
};

typedef Vec<float> Vec3f;
typedef Vec<double> Vec3d;

#endif /*VEC_H_*/
