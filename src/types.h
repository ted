#ifndef TYPES_H_
#define TYPES_H_

#include <GL/gl.h>
#include <math.h>

struct point
{
    GLfloat x,y,z,w;
};

inline static void point_add(struct point *dest, const struct point *src)
{
    dest->x += src->x;
    dest->y += src->y;
    dest->z += src->z;
    dest->w += src->w;
}

inline static void point_cross(struct point *dest, const struct point *a, const struct point *b)
{
    dest->x = a->y*b->z - a->z*b->y;
    dest->y = a->z*b->x - a->x*b->z;
    dest->z = a->x*b->y - a->y*b->x;
}

inline static void point_norm(struct point *dest)
{
    float d = sqrt(dest->x*dest->x + dest->y*dest->y + dest->z*dest->z) + 0.000001;
    dest->x /= d;
    dest->y /= d;
    dest->z /= d;
    dest->w = d;
}

#endif
