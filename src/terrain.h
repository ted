#ifndef TERRAIN_H_
#define TERRAIN_H_

#include <GL/gl.h>
#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif 

enum TERRAIN_MODE { FLAT, NORMAL, NORMAL_DEBUG };

struct terrain
{
    int size;
    float max_height, min_height;

    GLfloat *height;
    struct point *normal;

    int dirty;
    unsigned int seed;

    enum TERRAIN_MODE drawmode;
    GLuint drawlist;
};

struct index
{
    int i,j;
};

struct terrain* terrain_create(int size);

void terrain_destroy(struct terrain* t);

void terrain_bake(struct terrain *t);

void terrain_draw(struct terrain *t);

void terrain_smooth(struct terrain *t, float k);

void terrain_clear(struct terrain *t);

void terrain_fractal(struct terrain *t, float magnitude);

void terrain_plasma(struct terrain *t);

#ifdef __cplusplus
}
#endif 

#endif

